# vuex-arcgis-js-api-sample

This is sample of how components from the [ArcGIS JavaScript API](https://developers.arcgis.com/javascript/) can be tied to the [VueX](https://vuex.vuejs.org/) state.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
