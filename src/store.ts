import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    zoom: 11,
    center: [-87.65, 41.88]
  },
  mutations: {
    zoomIn(state) {
      state.zoom++;
    },
    zoomOut(state) {
      state.zoom--;
    }
  },
  actions: {

  },
});
